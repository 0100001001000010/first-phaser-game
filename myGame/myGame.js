/*global Phaser*/

var game = new Phaser.Game(800, 600, Phaser.AUTO, '');
var game_state = {}

game_state.main = function () {};
game_state.main.prototype = {

preload: function() {
    game.load.image('sky','assets/sky.png');
    game.load.image('ground','assets/platform.png');
    game.load.image('star','assets/star.png');
    game.load.spritesheet('dude','assets/dude.png', 32, 48);
},

create: function() {
    game.add.sprite(0, 0, 'sky');
    game.add.sprite(0, 0, 'star');
    
    this.platforms = game.add.group();                                          //The two ledges
    this.platforms.enableBody = true;                                           //Enable physics
    var ground = this.platforms.create (0, game.world.height - 64, 'ground');   //Create the ground
    ground.scale.setTo(2, 2);                                                   //Scale the ground
    ground.body.immovable = true;                                               //The ground can no longer move.
    var ledgeLeft = this.platforms.create(0, 0, 'ground');
    ledgeLeft.scale.setTo(0.05, 17.75);
    ledgeLeft.body.immovable = true;
    var ledgeRight = this.platforms.create(780, 0, 'ground');
    ledgeRight.scale.setTo(0.05, 17.75);
    ledgeRight.body.immovable = true;
    
    game.physics.startSystem(Phaser.Physics.ARCADE);
    
    //Setting up the player
    this.player = game.add.sprite (32, game.world.height - 150, 'dude');
    game.physics.arcade.enable(this.player);
    this.player.body.bounce.y = 0.9;
    this.player.body.gravity.y = 980;
    this.player.body.bounce.x = 0.5;
    this.player.body.collideWorldBounds = true;
    //The two animations
    this.player.animations.add('left', [0, 1, 2, 3], 10, true);
    this.player.animations.add('right',[5, 6, 7, 8], 10, true);
    
    this.stars = game.add.group();
    this.stars.enableBody = true;
    for (var i = 0; i < 12; i++) {
        var star = this.stars.create(i*70, 0, 'star');
        star.body.gravity.y = 300;
        star.body.bounce.y = 0.7 + Math.random() * 0.2;
    }
    
    this.scoreText = game.add.text (16, 16, 'score: 0', {fontSize: '32px', fill: '#000'});
    this.score = 0;
},

update: function() {
    game.physics.arcade.collide(this.player, this.platforms);
    game.physics.arcade.collide(this.stars, this.platforms);
    this.cursors = game.input.keyboard.createCursorKeys();
    if (this.cursors.left.isDown){
        this.player.body.velocity.x = -500;
        this.player.animations.play('left');
    } else if (this.cursors.right.isDown){
        this.player.body.velocity.x = 500;
        this.player.animations.play('right');
    } else {
        this.player.body.velocity.x = 0;
        this.player.animations.stop();
        this.player.frame = 4;
    }
    if (this.cursors.up.isDown && this.player.body.touching.down) {
        this.player.body.velocity.y = -750;
    }
    
    game.physics.arcade.overlap(this.player, this.stars, this.collectStar, null, this);
},

collectStar: function (player, star) {
    star.kill();
    this.score++;
    this.scoreText.text = "Score: " + this.score;
}
};
game.state.add('main', game_state.main);
game.state.start('main');
